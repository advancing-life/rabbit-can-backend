# frozen_string_literal: true

# SessionOauth is ...
class SessionOauth
  def self.user(hoge)
    user = User.find_by(mail: hoge['mail'])

    if user && user.u_id == hoge['u_id']
      return user
    else
      return false
    end
  end
end
