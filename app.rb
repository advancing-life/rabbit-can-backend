# frozen_string_literal: true

require 'bundler/setup'
Bundler.require
require 'sinatra/reloader' if development?
require 'pry' if development?

require './models'
require './session'

before do
  content_type :json
  headers 'Access-Control-Allow-Origin' => '*',
          'Access-Control-Allow-Methods' => %w[OPTIONS GET POST]
end

get '/' do
  status 200
end

post '/check_id' do
  result = SessionOauth.user(JSON.parse(request.body.read))

  if result
    body({
      u_id: result.u_id,
      mail: result.mail,
      name: result.name
    }.to_json)
    status 201
  else
    status 401
  end
end

post '/test' do
end

post '/sign_up' do
  result = User.new_create_user(JSON.parse(request.body.read))

  if result.errors.messages.empty?
    body({
      u_id: result.u_id,
      mail: result.mail,
      name: result.name
    }.to_json)
    status 201
  else
    body result.errors.to_json
    status 409
  end
end

post '/sign_in' do
  sign_in_data = JSON.parse(request.body.read)
  in_mail = sign_in_data['mail']
  in_pass = sign_in_data['password']

  result = User.oauth_user(in_mail, in_pass)

  if result
    user_data = {
      u_id: result.u_id,
      mail: result.mail,
      name: result.name
    }
    body(user_data.to_json)
    status 201
  else
    body({ msg: 'メールアドレス または、パスワード が違います。ご確認の上もう一度お試しください。' }.to_json)
    status 401
  end
end

get '/show' do
  article = {
    id: 1,
    title: "today's dialy",
    content: "It's a sunny day."
  }
  article.to_jsn
end
